﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MySql.Data.MySqlClient;
namespace WpfApp1
{
    /// <summary>
    /// Логика взаимодействия для CPU.xaml
    /// </summary>
    
    public partial class CPU : Page
    {
        string TableList;
        int User_ID;
        string DisplayName;
        public CPU(string TableName, int ID, int userid)
        {
            TableList = TableName;
            User_ID = userid;
            int DB_ERROR = 0;
            InitializeComponent();
            // строка подключения к БД
            string connStr = "server=localhost;user=admin;database=Computer_Accessories;password=admin;";
            // создаём объект для подключения к БД
            MySqlConnection conn = new MySqlConnection(connStr);
            // устанавливаем соединение с БД

            try
            {
                conn.Open();
            }
            catch
            {
                DB_ERROR = 1;
            }
            if (DB_ERROR == 0)
            {

                // запрос
                string sql = "SELECT * FROM "+TableName+" WHERE ID=" + ID;
                // объект для выполнения SQL-запроса
                MySqlCommand command = new MySqlCommand(sql, conn);
                // объект для чтения ответа сервера
                MySqlDataReader reader = command.ExecuteReader();
                // читаем результат


                while (reader.Read())
                {
                    
                    var imgUrl = new Uri(reader["Image_URL"].ToString()); // ссылка на картинку
                    var imageData = new WebClient().DownloadData(imgUrl);
                    var bitmapImage = new BitmapImage { CacheOption = BitmapCacheOption.OnLoad };
                    bitmapImage.BeginInit();
                    bitmapImage.StreamSource = new MemoryStream(imageData);
                    bitmapImage.EndInit();
                    Icon.Source = bitmapImage;
                    ListItemText.Text = reader["Name"].ToString();
                    page.Title = reader["Name"].ToString();
                    switch (TableName)
                    {
                        case "CPU":
                            Socket.Text = "Общая информация: Сокет " + reader["Socket"].ToString(); //Строка 1
                            Core_and_L3.Text = reader["Core_and_L3"].ToString();    //Строка 2
                            Frequency_and_Boost.Text = reader["Frequency_and_Boost"].ToString();    //Строка 3
                            TDP.Text = "Тепловые характеристики: " + reader["TDP"].ToString();  //Строка 4
                            GPU.Text = "Графические характеристики: " + reader["GPU"].ToString();   //Строка 5
                            break;
                        case "MOTHERBOARD":
                            Socket.Text = "Общая информация: Сокет " + reader["Socket"].ToString();
                            Core_and_L3.Text = "Форм-фактор и размеры: " + reader["Form"].ToString();
                            Frequency_and_Boost.Text = "Чипсет: " + reader["Chipset"].ToString();
                            TDP.Text = "Память: " + reader["Memory"].ToString();
                            GPU.Text = "Слоты расширения: " + reader["Slots"].ToString();
                            break;
                        default: //тут мне стало лень. Вместо того что бы как вверху делать каждому отдельную категорию с уникальными строками, все будет в куче.
                            Socket.Text = reader["String1"].ToString();
                            Core_and_L3.Text = reader["String2"].ToString();
                            Frequency_and_Boost.Text = reader["String3"].ToString();
                            TDP.Text = reader["String4"].ToString();
                            GPU.Text = reader["String5"].ToString();
                            break;
                    }
                    
                    Description.Text = reader["Description"].ToString();

                }

                reader.Close(); // закрываем reader
                                // закрываем соединение с БД
                conn.Close();



            }
        }

            private void Button_Click(object sender, RoutedEventArgs e)
        {
            WindowContentManager.SetPage(new Page1(User_ID));
        }

        private void TO_CART(object sender, RoutedEventArgs e)
        {

        }
    }
}
