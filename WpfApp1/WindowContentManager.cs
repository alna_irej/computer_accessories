﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace WpfApp1
{
    public static class WindowContentManager
    {
        private static bool _isInit = false;
        private static Window _mainWindow;

        public static void Init(Window mainWindow)
        {
            _mainWindow = mainWindow;
            _isInit = true;
        }

        private static void CheckInit()
        {
            if (!_isInit) throw new Exception("Менеджер окон не инициализирован");
        }

        public static void SetPage(Page page)
        {
            CheckInit();
            _mainWindow.Content = page;
            _mainWindow.Title = page.Title;
        }
    }
}
