﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Логика взаимодействия для Cart.xaml
    /// </summary>
    public partial class Cart : Page
    {
        string OrderList;
        int UserID;
        public Cart(string OrderBy, int User)
        {


            int DB_ERROR = 0;
                OrderList = OrderBy;
                UserID = User;
                InitializeComponent();
                // строка подключения к БД
                string connStr = "server=localhost;user=admin;database=Computer_Accessories;password=admin;";
                // создаём объект для подключения к БД
                MySqlConnection conn = new MySqlConnection(connStr);
                // устанавливаем соединение с БД

                try
                {
                    conn.Open();
                }
                catch
                {
                    DB_ERROR = 1;
                }
                if (DB_ERROR == 0)
                {
                    string sql = "SELECT * FROM CART";
                    // объект для выполнения SQL-запроса
                    MySqlCommand command = new MySqlCommand(sql, conn);
                    // объект для чтения ответа сервера
                    MySqlDataReader reader2 = command.ExecuteReader();
                    while (reader2.Read())
                    {
                        if (Convert.ToInt32(reader2["ID"]) == 1 && Convert.ToInt32(reader2["COMPLETE"]) == 0)
                        {

                        }
                    }
                    reader2.Close();
                    // запрос
                    sql = "SELECT Cart FROM CART WHERE USER = '"+UserID+"' AND COMPLETE = 0 ORDER BY " + OrderBy;
                    // объект для выполнения SQL-запроса
                    command = new MySqlCommand(sql, conn);
                    // объект для чтения ответа сервера
                    MySqlDataReader reader = command.ExecuteReader();
                    // читаем результат

                    int j = 0;
                    while (reader.Read())
                    {
                        Grid myGrid = new Grid()
                        {
                            Name = "Grid" + j
                        };


                        // Define the Columns
                        ColumnDefinition colDef1 = new ColumnDefinition() { Width = new System.Windows.GridLength(100) }; ;
                        ColumnDefinition colDef2 = new ColumnDefinition() { Width = new System.Windows.GridLength(1, System.Windows.GridUnitType.Star) }; ;
                        ColumnDefinition colDef3 = new ColumnDefinition() { Width = new System.Windows.GridLength(0.2, System.Windows.GridUnitType.Star) }; ;
                        myGrid.ColumnDefinitions.Add(colDef1);
                        myGrid.ColumnDefinitions.Add(colDef2);
                        myGrid.ColumnDefinitions.Add(colDef3);

                        // Define the Rows
                        RowDefinition rowDef1 = new RowDefinition() { Height = new System.Windows.GridLength(50) };
                        RowDefinition rowDef2 = new RowDefinition() { Height = new System.Windows.GridLength(50) };
                        myGrid.RowDefinitions.Add(rowDef1);
                        myGrid.RowDefinitions.Add(rowDef2);
                        var imgUrl = new Uri(reader["Image_URL"].ToString());
                        var imageData = new WebClient().DownloadData(imgUrl);

                        // or you can download it Async won't block your UI
                        // var imageData = await new WebClient().DownloadDataTaskAsync(imgUrl);

                        var bitmapImage = new BitmapImage { CacheOption = BitmapCacheOption.OnLoad };
                        bitmapImage.BeginInit();
                        bitmapImage.StreamSource = new MemoryStream(imageData);
                        bitmapImage.EndInit();
                        Image Icon = new Image()
                        {
                            Stretch = Stretch.Fill,
                            Source = bitmapImage,
                            Cursor = Cursors.Hand,
                            Uid = Convert.ToString(reader["ID"].ToString())

                        };
                        //Icon.MouseUp += Image_MouseUp;
                        Grid.SetColumn(Icon, 0);
                        Grid.SetColumnSpan(Icon, 1);
                        Grid.SetRow(Icon, 0);
                        Grid.SetRowSpan(Icon, 2);

                        myGrid.Children.Add(Icon);

                        TextBlock Label = new TextBlock()
                        {
                            Text = reader["Name"].ToString(),
                            FontFamily = new FontFamily("Arial Rounded MT Bold"),
                            FontSize = 36
                        };
                        Grid.SetColumn(Label, 1);
                        Grid.SetColumnSpan(Label, 1);
                        Grid.SetRow(Label, 0);
                        Grid.SetRowSpan(Label, 1);
                        myGrid.Children.Add(Label);


                        TextBlock Short_description = new TextBlock()
                        {
                            Text = reader["Short_description"].ToString(),
                            FontFamily = new FontFamily("Arial Rounded MT Bold"),
                            FontSize = 18
                        };
                        Grid.SetColumn(Short_description, 1);
                        Grid.SetColumnSpan(Short_description, 1);
                        Grid.SetRow(Short_description, 1);
                        Grid.SetRowSpan(Short_description, 1);
                        myGrid.Children.Add(Short_description);

                        TextBlock Cost = new TextBlock()
                        {
                            Background = new SolidColorBrush(Color.FromRgb(238, 115, 45)),
                            Foreground = new SolidColorBrush(Colors.White),
                            Text = reader["Cost"].ToString() + " руб",
                            Margin = new Thickness(5, 5, 5, 5),
                            FontFamily = new FontFamily("Arial Rounded MT Bold"),
                            FontSize = 36
                        };
                        Grid.SetColumn(Cost, 2);
                        Grid.SetColumnSpan(Cost, 1);
                        Grid.SetRow(Cost, 0);
                        Grid.SetRowSpan(Cost, 1);
                        myGrid.Children.Add(Cost);

                        Button cart_add = new Button()
                        {
                            Background = new SolidColorBrush(Colors.Green),
                            Foreground = new SolidColorBrush(Colors.White),
                            Content = "Добавить",
                            //Margin = new Thickness(5, 5, 5, 5),
                            FontFamily = new FontFamily("Arial Rounded MT Bold"),
                            FontSize = 36
                        };
                        //cart_add.MouseUp += CART_ADD;
                        Grid.SetColumn(cart_add, 2);
                        Grid.SetColumnSpan(cart_add, 1);
                        Grid.SetRow(cart_add, 1);
                        Grid.SetRowSpan(cart_add, 1);
                        myGrid.Children.Add(cart_add);

                        ScroolGrid.Children.Add(myGrid);


                        //Image.Height = 100;
                        //image.Width = 100;
                        //Console.WriteLine(reader[0].ToString() + " " + reader[1].ToString());
                        j++;
                    }
                    //ListItemText.Text = TableName + " " + j + " вариантов";
                    reader.Close(); // закрываем reader
                                    // закрываем соединение с БД
                    conn.Close();

                    switch (OrderBy)
                    {
                        case "ID":

                            _default.IsSelected = true;
                            break;
                        case "Name":

                            name_up.IsSelected = true;
                            break;
                        case "Name DESC":

                            name_down.IsSelected = true;
                            break;
                        case "Cost":

                            cost_up.IsSelected = true;
                            break;
                        case "Cost DESC":

                            cost_down.IsSelected = true;
                            break;

                    }
                }
            }

        }
}
