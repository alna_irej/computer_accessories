﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Логика взаимодействия для Page1.xaml
    /// </summary>
    public partial class Page1 : Page
    {
        int UserID;
        public Page1(int ID)
        {
            UserID = ID;
            InitializeComponent();


        }

        private void _01_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void _03_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {

        }

        private void _03_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            WindowContentManager.SetPage(new List("CPU","ID",UserID));
        }

        private void _05_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            WindowContentManager.SetPage(new List("MOTHERBOARD","ID",UserID));
        }

        private void _01_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            WindowContentManager.SetPage(new List("PLATFORMS", "ID", UserID));
        }

        private void _02_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            WindowContentManager.SetPage(new List("HDD", "ID", UserID));
        }

        private void _04_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            WindowContentManager.SetPage(new List("COOLING", "ID", UserID));
        }

        private void _06_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            WindowContentManager.SetPage(new List("MEMORY", "ID", UserID));
        }

        private void _07_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            WindowContentManager.SetPage(new List("GPU", "ID", UserID));
        }

        private void _08_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            WindowContentManager.SetPage(new List("POWER", "ID", UserID));
        }

        private void _09_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            WindowContentManager.SetPage(new List("AUDIO", "ID", UserID));
        }

        private void _010_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            WindowContentManager.SetPage(new List("INTERNET", "ID", UserID));
        }

        private void _011_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            WindowContentManager.SetPage(new List("BLOCKS", "ID", UserID));
        }

        private void _012_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            WindowContentManager.SetPage(new List("OTHER", "ID", UserID));
        }
    }
}
