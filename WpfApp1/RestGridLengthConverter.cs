﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace WpfApp1
{
    class RestGridLengthConverter : IValueConverter
    {
        public object Convert(object value, Type tt, object parameter, CultureInfo ci)
        {
            return new GridLength((double)value - 1, GridUnitType.Star);
        }

        public object ConvertBack(object value, Type tt, object parameter, CultureInfo ci)
        {
            throw new NotImplementedException();
        }
    }
}
